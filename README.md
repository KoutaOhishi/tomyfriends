To My Friends

## Description  
 S教新聞で毎日掲載されている「わが友に贈る」の画像を取得して、LINEやTwitterに投稿します。  
 [Twitter](https://twitter.com/ValueCreator316)

## Comment
 LINEやTwitterに投稿するにはそれぞれアクセストークンを取得する必要があります。  
 予め用意しておいてください。  

get_img.pyで記事の画像を保存、send.pyで投稿することができます。  

保存先のパスや、アクセストークン等を書き換えておいてください。　　

Ubuntu16.04で実行済み
