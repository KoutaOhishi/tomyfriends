# coding: utf-8
#import webbrowser
import os
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import datetime
import requests

def main():
    date = datetime.date.today()

    URL = "http://www.seikyoonline.com/news/wagatomo/"

    #FILENAME = os.path.join(os.path.dirname(os.path.abspath(__file__)), "screen.png")

    #PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
    #DRIVER_BIN = os.path.join(PROJECT_ROOT, "/home/robotgroup/Everyday_wagatomo/chromedriver")
    print URL
    print "Load webdriver" + str(date)

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(executable_path='/PATH/TO/chromedriver',chrome_options=chrome_options)
    #driver = webdriver.Chrome(DRIVER_BIN)

    print "open Google Chrome"
    driver.get(URL)

    #print(driver.get_window_rect())


    driver.find_element_by_class_name("bl-bigger").click()


    driver.find_element_by_class_name("type_d_main").click()


    #sleep(5)
    driver.switch_to.window(driver.window_handles[1])
    cur_url = driver.current_url
    #print str(cur_url)

    driver.set_window_size(900,1600)

    print "screen shot"
    driver.save_screenshot("/PATH/TO/input/"+str(date)+".png")


    print "cur_url:" + str(cur_url)
    response = requests.get(cur_url, stream=True)
    filename = "test.jpeg"
    with open(filename, 'wb') as file:
                for chunk in response.iter_content(chunk_size=1024):
                    file.write(chunk)

    print "close Google Chrome"
    #driver.close()
    driver.quit()

if __name__ == "__main__":
    main()
