# coding: utf-8
#import webbrowser
import os, json
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import datetime
import requests
from PIL import Image
from requests_oauthlib import OAuth1Session

Date = datetime.date.today()
Web_url = "http://www.seikyoonline.com/news/wagatomo/"
Api_url = "https://notify-api.line.me/api/notify"
Token = "XXXXXXXXXXXXXXXXXXXXXXXX" #自分で取得

DRIVER_BIN = "/PATH/TO/chromedriver"
FILENAME = "/PATH/TO/output/"+str(Date)+".jpeg"

def web():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(executable_path=DRIVER_BIN,chrome_options=chrome_options)

    print "open Google Chrome"
    driver.get(Web_url)

    driver.find_element_by_class_name("bl-bigger").click()
    driver.find_element_by_class_name("type_d_main").click()

    #sleep(5)
    driver.switch_to.window(driver.window_handles[1])
    cur_url = driver.current_url

    response = requests.get(cur_url, stream=True)
    with open(FILENAME, 'wb') as file:
        for chunk in response.iter_content(chunk_size=1024):
            file.write(chunk)

    print "close Google Chrome"
    #driver.close()
    driver.quit()

def line():
    headers = {"Authorization" : "Bearer " + Token}
    message =  "[" + str(Date) + "]" + ' わが友に贈る'
    payload = {"message" : message}
    files = {"imageFile": open(FILENAME, "rb")}

    r = requests.post(Api_url ,headers = headers ,params=payload, files=files)

    print str(Date)+" Sent to Line Notify"

def tweet():
    CK = "XXXXXXXXXXXXXXXXXXXXXXXX" #CONSUMER_KEY
    CS = "XXXXXXXXXXXXXXXXXXXXXXXX" #CONSUMER_SECRET
    AT = "XXXXXXXXXXXXXXXXXXXXXXXX" #ACCESS_TOKEN
    ATS = "XXXXXXXXXXXXXXXXXXXXXXXX" #ACCESS_TOKEN_SECRET
    twitter = OAuth1Session(CK, CS, AT, ATS)

    url_media = "https://upload.twitter.com/1.1/media/upload.json"
    url_text = "https://api.twitter.com/1.1/statuses/update.json"

    files = {"media" : open(FILENAME, 'rb')}
    req_media = twitter.post(url_media, files = files)

    if req_media.status_code != 200:
        print("MEDIA UPLOAD FAILED... %s", req_media.text)
        exit()

    media_id = json.loads(req_media.text)['media_id']

    text = "【"+str(Date)+"】\n#わが友に贈る" #投稿するメッセージ
    params = {"status" : text, "media_ids" : [media_id]}
    req_media = twitter.post(url_text, params = params)

    if req_media.status_code != 200:
        print("TEXT UPLOAD FAILED... %s", req_text.text)
        exit()

    print "Tweeted!"


def main():
    web()
    line()
    tweet()

if __name__ == "__main__":
    main()
